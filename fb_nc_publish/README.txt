-- SUMMARY --

User will have a panel to administrate the Facebook publish setting,
this panel will contain all the content type that an authenticated user could create 
the user can select wich content type will be directly published to Facebook after he add a ny content
also this panel will have the setting to publish on Facebook the like and share activity on node

on every content page we want to have a checkbox to publish on Facebook 
this checkbox will be checked if the user has checked the content type from the administration page

user have to option to uncheck or check this button


-- DEPENDENCIES --
Facebook Connect: http://drupal.org/project/fbconnect enable and configure fbconnect module
Content Construction Kit (CCK): http://drupal.org/project/cck 


-- INSTALLATION --
  1. Upload the 'fb_nc_publish' folder into your module directory or module/custom directory and activate the module,

  2. Upload facebook-php-sdk (http://github.com/facebook/php-sdk/) into fb_nc_publish module folder (or into libraries folder,
    if you want to use it with the libraries module, http://drupal.org/project/libraries)

     To ensure that you installed correctly check file : <fb_nc_publish folder>/facebook-php-sdk/src/facebook.php must be accessible
     OR
     To ensure that you installed correctly check file : sites/all/libraries/facebook-php-sdk/src/facebook.php must be accessible

  3. Allow access permission for user from http://website.com/admin/user/permissions,
     Admin can allow permission according to user Role and after that authenticate user can access the Facebook Publish setting. 
     Permission: "Facebook publish configuration"

  4. Facebook Publish General settings: http://website.com/fb_nc_publish/settings/list
     4.1 Authorise User can select content type which will be directly published to Facebook,
         Publish to facebook checkbox will be display on node page and it is by default checked for selected content type of user.
     4.2 Authorise User can enable and disabled "Like and Share activity publish on Facebook"

  5. Facebook Publish content fields settings: http://website.com/fb_nc_publish/settings/content_fields_publish
     Select the fields of content type for publish to Facebook,
     By default node title will be publish for node

     Like Node type: "abc" => 
     fields 	1. title, 
            	2. body, 
            	3. Image, 
		4. description
                6. others 

if you select all the fields than all the fields will be publish on Facebook
--------------------------
Title
--------------------------
Image | body + description + others (all text fields)
--------------------------



Like & Share Activity post on Facebook
------------------------------------------
Add new rules admin/rules/trigger
1.Post like activity on facebook
	Label = "<Post like activity on facebook>"
	Event = select <A node has been flaged, under "Like a Node"> 
Add a condition
	Select <Execute custom PHP code>
	Put php code here =>   fb_nc_publish_message([node:title]);


2.Post share activity on facebook
	Label = "<Post share activity on facebook>"
	Event = select <A node has been flaged, under "Share"> 
Add a condition
	Select <Execute custom PHP code>
	Put php code here =>   fb_nc_publish_message([node:title]);





